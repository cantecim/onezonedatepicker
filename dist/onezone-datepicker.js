!function(n,e){var o=n.createElement("style");if(n.getElementsByTagName("head")[0].appendChild(o),o.styleSheet)o.styleSheet.disabled||(o.styleSheet.cssText=e);else try{o.innerHTML=e}catch(t){o.innerText=e}}(document,'.onezone-datepicker {\n  position: relative;\n  width: 100%;\n  display: block;\n  background-color: #FBFAFA;\n  color: #3A4351;\n  overflow: hidden;\n  max-height: 0;\n  transition: max-height .4s; }\n\n.onezone-datepicker.onezone-datepicker-show {\n  max-height: 600px;\n  border-bottom: 2px solid #ECECEC; }\n\n.onezone-datepicker-modal {\n  position: absolute;\n  z-index: 1;\n  top: 0;\n  bottom: 0;\n  left: 0;\n  right: 0;\n  text-align: center;\n  background: rgba(255, 255, 255, 0.9); }\n\n.onezone-datepicker-loader {\n  position: absolute;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n  -ms-transform: translateY(-50%);\n  transform: translateY(-50%);\n  margin-left: -10px; }\n\n.onezone-datepicker-loader svg {\n  width: 20px;\n  height: 20px; }\n\n.onezone-datepicker-button {\n  position: relative;\n  text-align: center;\n  display: inline-block;\n  cursor: pointer;\n  color: #3A4351;\n  font-size: 15px;\n  font-weight: bold;\n  margin-bottom: 5px;\n  padding: 8px 0;\n  border-bottom: 1px solid #F48685;\n  padding: 5px; }\n\n.onezone-datepicker-button:after {\n  position: absolute;\n  content: " ";\n  width: 0;\n  height: 0;\n  right: 0;\n  bottom: 0;\n  border-bottom: 6px solid #F48685;\n  border-left: 6px solid transparent; }\n\n.onezone-datepicker-select-year {\n  position: absolute;\n  top: 8px;\n  left: 5px;\n  width: 25%; }\n\n.onezone-datepicker-navigation-arrow {\n  display: inline-block;\n  cursor: pointer;\n  font-size: 24px;\n  color: #F48685;\n  text-align: center;\n  width: 24px;\n  padding-top: 10px;\n  line-height: 24px; }\n\n.onezone-datepicker-week {\n  font-weight: 600;\n  font-size: 13px;\n  text-align: center;\n  margin-bottom: 0px;\n  border-bottom: 1px solid #dddddd;\n  padding: px 0; }\n\n.onezone-datepicker-day {\n  -webkit-border-radius: 100%;\n  -moz-border-radius: 100%;\n  -ms-border-radius: 100%;\n  border-radius: 100%;\n  cursor: pointer;\n  display: block;\n  margin: 0 auto;\n  text-align: center;\n  font-size: 12px;\n  width: 26px;\n  height: 26px;\n  line-height: 22px;\n  border: 2px solid #FBFAFA; }\n\n.onezone-datepicker-current-day {\n  border: 2px solid #dddddd; }\n\n.onezone-datepicker-disable-day,\n.onezone-datepicker-different-month.onezone-datepicker-disable-day {\n  color: #dddddd; }\n\n.onezone-datepicker-different-month {\n  color: #afafaf; }\n\n.onezone-datepicker-active-day,\n.onezone-datepicker-active-day:hover {\n  border: 2px solid #F48685;\n  font-weight: bold; }\n\n.onezone-datepicker-selection-button {\n  color: #F48685;\n  font-size: 34px;\n  padding-top: 5px;\n  display: inline-block;\n  cursor: pointer;\n  width: 40px; }\n\n.onezone-datepicker-close-button {\n  position: absolute;\n  right: 10px;\n  top: 10px;\n  z-index: 1;\n  font-size: 28px;\n  cursor: pointer;\n  color: #3A4351;\n  width: 50px;\n  text-align: right; }\n\n.onezone-datepicker-modal-content {\n  position: relative;\n  padding: 50px 5px 40px 5px; }\n\n.onezone-datepicker-modal-content .slider {\n  position: initial; }\n\n.onezone-datepicker-modal-content:after {\n  content: "";\n  display: table;\n  clear: both; }\n\n.onezone-datepicker-modal-item {\n  display: block;\n  float: left;\n  width: 25%;\n  line-height: 50px;\n  font-size: 14px;\n  text-transform: uppercase;\n  cursor: pointer; }\n\n.onezone-datepicker-modal-item.onezone-datepicker-modal-item-active {\n  background-color: #F48685;\n  color: white;\n  font-weight: bold; }');
!function(e){try{e=angular.module("onezone-datepicker.templates")}catch(n){e=angular.module("onezone-datepicker.templates",[])}e.run(["$templateCache",function(e){e.put("onezone-datepicker.html",'<div class=onezone-content><div class=onezone-transclude><ng-transclude></ng-transclude></div><div class=onezone-datepicker ng-class="{ \'onezone-datepicker-show\' : datepicker.showDatepicker }"><div class=onezone-datepicker-modal ng-if="datepicker.showLoader || datepicker.showMonthModal || datepicker.showYearModal"><div class="onezone-datepicker-close-button icon ion-ios-close-outline" ng-if="datepicker.showMonthModal || datepicker.showYearModal" ng-click=closeModals()></div><ion-spinner icon=spiral class="spinner-dark onezone-datepicker-loader" ng-if=datepicker.showLoader></ion-spinner><div class=onezone-datepicker-modal-content ng-if=datepicker.showMonthModal><div class="onezone-datepicker-button onezone-datepicker-select-year" ng-click=openYearModal()>{{currentMonth.getFullYear()}}</div><div class=onezone-datepicker-modal-item ng-repeat="m in months track by $index" ng-class="{\'onezone-datepicker-modal-item-active\': currentMonth.getMonth() == $index}" ng-click=selectMonth($index)>{{m.substr(0,3)}}</div></div><div class=onezone-datepicker-modal-content ng-if=datepicker.showYearModal><ion-slide-box show-pager=false active-slide=selectedYearSlide><ion-slide ng-repeat="slide in yearSlides"><div class=onezone-datepicker-modal-item ng-repeat="year in slide.years" ng-click=selectYear(year) ng-class="{\'onezone-datepicker-modal-item-active\': currentMonth.getFullYear() == year}">{{year}}</div></ion-slide></ion-slide-box></div></div><div class=row><div class=col-25><span class="onezone-datepicker-navigation-arrow ion-ios-arrow-back" ng-click=previousMonth()></span></div><div class="col text-center"><div class=onezone-datepicker-button ng-click=openMonthModal()>{{months[currentMonth.getMonth()]}} {{currentMonth.getFullYear()}}</div></div><div class="col-25 text-right"><span class="onezone-datepicker-navigation-arrow ion-ios-arrow-forward" ng-click=nextMonth()></span></div></div><div class=row><div class="col onezone-datepicker-week" ng-repeat="dayOfTheWeek in daysOfTheWeek track by $index">{{ dayOfTheWeek }}</div></div><div class=row ng-repeat="week in month" on-swipe-right=swipeRight() on-swipe-left=swipeLeft()><div class=col ng-repeat="day in week.days" ng-click="selectDate(day.fullDate, day.isDisabled)"><div class=onezone-datepicker-day ng-style="{ \'background-color\': day.highlight.color, \'color\': day.highlight.textColor }" ng-class="{ \'onezone-datepicker-current-day\' : day.isToday, \'onezone-datepicker-different-month\' : !day.isCurrentMonth, \'onezone-datepicker-active-day\' : sameDate(day.fullDate, selectedDate), \'onezone-datepicker-disable-day\' : day.isDisabled }">{{day.date}}</div></div></div><div class=row ng-if=!datepicker.calendarMode><div class=col><span class="onezone-datepicker-selection-button ion-ios-calendar-outline" ng-if=datepicker.showTodayButton ng-click=selectToday()></span></div><div class="col text-right"><span class="onezone-datepicker-selection-button ion-ios-close-outline" ng-if=!datepicker.hideCancelButton ng-click=hideDatepicker()></span> <span class="onezone-datepicker-selection-button ion-ios-checkmark" ng-if=!datepicker.hideSetButton ng-click=setDate()></span></div></div></div></div>')}])}();
angular.module('onezone-datepicker.service', ['ionic'])
    .factory('onezoneDatepickerService', function () {
        'use strict';

        var serviceFactory = {};

        function datesAreEquals(date, compareDate) {
            if (angular.isDefined(date) && angular.isDate(date) && angular.isDefined(compareDate) && angular.isDate(compareDate)) {
                return date.getDate() === compareDate.getDate() && date.getMonth() === compareDate.getMonth() && date.getFullYear() === compareDate.getFullYear();
            }

            return false;
        }

        /* Get start date for month (first day from first week of the month) */
        function getStartDate(date, mondayFirst) {
            var position, startDate = new Date(date.getFullYear(), date.getMonth(), 1);
            if ((!mondayFirst && (date.getDay() !== 0 || date.getDate() !== 1)) || (mondayFirst && (date.getDay() !== 1 || date.getDate() !== 1))) {
                position = startDate.getDay() - mondayFirst;
                position = mondayFirst && position < 0 ? 6 : position;
                startDate = new Date(date.getFullYear(), date.getMonth(), 1 - position);
            }

            return startDate;
        }

        function checkIfIsDisabled(date, disablePastDays, disableWeekend, disableDates, displayFrom, displayTo) {
            var compareDate, currentDate, day, disableDate, today = new Date();
            currentDate = new Date(today.getFullYear(), today.getMonth(), today.getDate());
            compareDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());

            if (disablePastDays) {
                if (compareDate < currentDate) {
                    return true;
                }
            }

            if (disableWeekend) {
                day = compareDate.getDay();
                if (day === 0 || day === 6) {
                    return true;
                }
            }

            if (angular.isDefined(displayFrom)) {
                if (compareDate < displayFrom) {
                    return true;
                }
            }

            if (angular.isDefined(displayTo)) {
                if (compareDate > displayTo) {
                    return true;
                }
            }

            if (angular.isDefined(disableDates) && angular.isArray(disableDates)) {
                for (var i = 0; i < disableDates.length; i++) {
                    disableDate = new Date(disableDates[i].getFullYear(), disableDates[i].getMonth(), disableDates[i].getDate());
                    if (datesAreEquals(disableDate, compareDate)) {
                        return true;
                    }
                }
            }

            return false;
        }

        function getHighlightColor(highlights, date) {
            if (angular.isDefined(highlights) && angular.isArray(highlights) && highlights.length > 0) {
                for (var i = 0; i < highlights.length; i++) {
                    var highlight = highlights[i];
                    if (angular.isDefined(highlight.date)) {
                        if (_sameDate(date, highlight.date)) {
                            var backgroundColor = '#F48685';
                            var textColor = '#fff';

                            if (angular.isDefined(highlight.color)) {
                                backgroundColor = highlight.color;
                            }

                            if (angular.isDefined(highlight.textColor)) {
                                textColor = highlight.textColor;
                            }

                            return {
                                color: backgroundColor,
                                textColor: textColor
                            };
                        }
                    }
                }
            }

            return null;
        }

        /* Create week */
        function createWeek(date, currentMonth, disablePastDays, disableWeekend, disableDates, displayFrom, displayTo, highlights) {
            var days = [];
            date = angular.copy(date);

            for (var i = 0; i < 7; i++) {
                days.push({
                    fullDate: date,
                    date: date.getDate(),
                    month: date.getMonth(),
                    year: date.getFullYear(),
                    day: date.getDay(),
                    isToday: _sameDate(date, new Date()),
                    isCurrentMonth: date.getMonth() === currentMonth.getMonth(),
                    isDisabled: checkIfIsDisabled(date, disablePastDays, disableWeekend, disableDates, displayFrom, displayTo),
                    highlight: getHighlightColor(highlights, date)
                });

                date = angular.copy(date);
                date.setDate(date.getDate() + 1);
            }

            return days;
        }

        var _getParameters = function (scope) {
            var callback, startYear, endYear, displayFrom, displayTo, mondayFirst = false,
                disableSwipe = false,
                disablePastDays = false,
                disableWeekend = false,
                showTodayButton = true,
                disableDates = [],
                showDatepicker = false,
                calendarMode = false,
                hideCancelButton = false,
                hideSetButton = false,
                highlights = [];

            /* MONDAY FIRST */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.mondayFirst)) {
                mondayFirst = scope.datepickerObject.mondayFirst;
            }

            /* GET DISABLE PAST DAYS FLAG  */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.disablePastDays)) {
                disablePastDays = scope.datepickerObject.disablePastDays;
            }

            /* GET DISABLE WEEKEND */
            if (angular.isDefined(scope.datepickerObject && angular.isDefined(scope.datepickerObject.disableWeekend))) {
                disableWeekend = scope.datepickerObject.disableWeekend;
            }

            /* GET DISABLE SWIPE */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.disableSwipe)) {
                disableSwipe = scope.datepickerObject.disableSwipe;
            }

            /* GET DISABLE DATES */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.disableDates) && angular.isArray(disableDates)) {
                disableDates = scope.datepickerObject.disableDates;
            }

            /* MONTHS */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.months) && angular.isArray(scope.datepickerObject.months) && scope.datepickerObject.months.length === 12) {
                scope.months = scope.datepickerObject.months;
            } else {
                scope.months = _getMonths();
            }

            /* DAYS OF THE WEEK */
            if (angular.isDefined(scope.datepickerObject)) {
                scope.daysOfTheWeek = _getDaysOfTheWeek(mondayFirst, scope.datepickerObject.daysOfTheWeek);

            } else {
                scope.daysOfTheWeek = _getDaysOfTheWeek(mondayFirst, null);
            }

            /* GET START DATE */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.startDate) && angular.isDate(scope.datepickerObject.startDate)) {
                startYear = scope.datepickerObject.startDate.getFullYear();
                displayFrom = scope.datepickerObject.startDate;
            } else {
                startYear = scope.currentMonth.getFullYear() - 120;
            }

            /* GET END DATE */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.endDate) && angular.isDate(scope.datepickerObject.endDate)) {
                endYear = scope.datepickerObject.endDate.getFullYear();
                displayTo = scope.datepickerObject.endDate;
            } else {
                endYear = scope.currentMonth.getFullYear() + 11;
            }

            /* GET SHOW CALENDAR FLAG */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.showDatepicker)) {
                showDatepicker = scope.datepickerObject.showDatepicker;
            }

            /* GET SHOW TODAY BUTTON FLAG */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.showTodayButton)) {
                showTodayButton = scope.datepickerObject.showTodayButton;
            }

            /* GET CALLBACK FUNCTION */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.callback) && angular.isFunction(scope.datepickerObject.callback)) {
                callback = scope.datepickerObject.callback;
            }

            /* GET CALENDAR MODE FLAG */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.calendarMode)) {
                calendarMode = scope.datepickerObject.calendarMode;
            }

            /* GET HIDE CANCEL BUTTON FLAG */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.hideCancelButton)) {
                hideCancelButton = scope.datepickerObject.hideCancelButton;
            }

            /* GET HIDE SET BUTTON FLAG */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.hideSetButton)) {
                hideSetButton = scope.datepickerObject.hideSetButton;
            }

            /* GET HIGHLIGHTS DATES */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.highlights) && angular.isArray(scope.datepickerObject.highlights)) {
                highlights = scope.datepickerObject.highlights;
            }

            return {
                mondayFirst: mondayFirst,
                startYear: startYear,
                endYear: endYear,
                displayFrom: displayFrom,
                displayTo: displayTo,
                disableSwipe: disableSwipe,
                disablePastDays: disablePastDays,
                disableWeekend: disableWeekend,
                disableDates: disableDates,
                showDatepicker: showDatepicker,
                showTodayButton: showTodayButton,
                calendarMode: calendarMode,
                hideCancelButton: hideCancelButton,
                hideSetButton: hideSetButton,
                highlights: highlights,
                callback: callback
            };
        };

        /* Get years method */
        var _getYears = function (startYear, endYear) {
            var count = 0;
            var yearSlides = [];
            var years = [];
            for (var i = startYear; i <= endYear; i++) {
                years.push(i);
                count++;
                if (count % 12 === 0) {
                    yearSlides.push({
                        years: years
                    });

                    years = [];
                }
            }

            if (years.length > 0) {
                yearSlides.push({
                    years: years
                });
            }

            return yearSlides;
        };

        /* Get active year slide */
        var _getActiveYearSlide = function (yearSlides, year) {
            if (angular.isDefined(yearSlides) && angular.isArray(yearSlides)) {
                for (var i = 0; i < yearSlides.length; i++) {
                    var index = yearSlides[i].years.indexOf(year);
                    if (index > -1) {
                        return i;
                    }
                }
            }

            return 0;
        };

        /* Get months */
        var _getMonths = function () {
            var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            return months;
        };

        /* Get weeks */
        var _getDaysOfTheWeek = function (mondayFirst, customDaysOfTheWeek) {
            var daysOfTheWeek = [];
            if (angular.isDefined(customDaysOfTheWeek) && angular.isArray(customDaysOfTheWeek) && customDaysOfTheWeek.length === 7) {
                daysOfTheWeek = angular.copy(customDaysOfTheWeek);
            } else {
                daysOfTheWeek = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
            }
            if (angular.isDefined(mondayFirst) && mondayFirst === true) {
                daysOfTheWeek.push(daysOfTheWeek.shift());
            }

            return daysOfTheWeek;
        };

        /* Get previous month */
        var _getPreviousMonth = function (date) {
            var previousYear = date.getFullYear(),
                previousMonth = date.getMonth() - 1;
            if (previousMonth < 0) {
                previousYear -= 1;
                previousMonth = 11;
            }

            return new Date(previousYear, previousMonth, 1);
        };

        /* Check if two dates are equal */
        var _sameDate = function (date, compareDate) {
            return datesAreEquals(date, compareDate);
        };

        /* Create month datepicker */
        var _createMonth = function (createMonthParam) {
            var stopflag = false,
                count = 0,
                weeks = [];
            var date = getStartDate(createMonthParam.date, createMonthParam.mondayFirst);
            var monthIndex = date.getMonth();

            while (!stopflag) {
                weeks.push({
                    days: createWeek(date, createMonthParam.date, createMonthParam.disablePastDays, createMonthParam.disableWeekend, createMonthParam.disableDates, createMonthParam.displayFrom, createMonthParam.displayTo, createMonthParam.highlights)
                });

                date.setDate(date.getDate() + 7);
                stopflag = count++ > 1 && monthIndex !== date.getMonth();
                monthIndex = date.getMonth();
            }

            return weeks;
        };

        var _showTodayButton = function (parameters) {
            var date = new Date();
            if (!parameters.showTodayButton || parameters.calendarMode) {
                return false;
            }

            return !checkIfIsDisabled(date, parameters.disablePastDays, parameters.disableWeekend, parameters.disableDates, parameters.displayFrom, parameters.displayTo);
        };

        serviceFactory.getParameters = _getParameters;
        serviceFactory.getYears = _getYears;
        serviceFactory.getActiveYearSlide = _getActiveYearSlide;
        serviceFactory.getMonths = _getMonths;
        serviceFactory.getDaysOfTheWeek = _getDaysOfTheWeek;
        serviceFactory.getPreviousMonth = _getPreviousMonth;
        serviceFactory.sameDate = _sameDate;
        serviceFactory.createMonth = _createMonth;
        serviceFactory.showTodayButton = _showTodayButton;
        return serviceFactory;
    });
angular.module('onezone-datepicker', ['ionic', 'onezone-datepicker.templates', 'onezone-datepicker.service'])
    .directive('onezoneDatepicker', ['$ionicGesture', 'onezoneDatepickerService', function ($ionicGesture, onezoneDatepickerService) {
        'use strict';

        function drawDatepicker(scope) {
            var selectedDate, parameters = {};

            /* SET SELECTED DATE */
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.date) && angular.isDate(scope.datepickerObject.date)) {
                selectedDate = angular.copy(scope.datepickerObject.date);
            } else {
                selectedDate = new Date();
            }

            scope.selectedDate = selectedDate;
            scope.currentMonth = angular.copy(selectedDate);

            parameters = onezoneDatepickerService.getParameters(scope);

            /* CREATE MONTH CALENDAR */
            scope.createDatepicker = function (date) {
                var createMonthParam = {
                    date: date,
                    mondayFirst: parameters.mondayFirst,
                    disablePastDays: parameters.disablePastDays,
                    displayFrom: parameters.displayFrom,
                    displayTo: parameters.displayTo,
                    disableWeekend: parameters.disableWeekend,
                    disableDates: parameters.disableDates,
                    highlights: parameters.highlights
                };

                return onezoneDatepickerService.createMonth(createMonthParam);
            };

            scope.month = scope.createDatepicker(scope.selectedDate);
            scope.yearSlides = onezoneDatepickerService.getYears(parameters.startYear, parameters.endYear);
            scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
            return parameters;
        }

        function showHideDatepicker(scope, value) {
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.showDatepicker)) {
                scope.datepickerObject.showDatepicker = value;
            } else {
                scope.datepicker.showDatepicker = value;
            }
        }

        function setDate(scope, parameters) {
            if (angular.isDefined(scope.datepickerObject) && angular.isDefined(scope.datepickerObject.date)) {
                scope.datepickerObject.date = scope.selectedDate;
            }

            if (!parameters.calendarMode) {
                showHideDatepicker(scope, false);
            }

            if (angular.isDefined(parameters.callback)) {
                parameters.callback(scope.selectedDate);
            }
        }

        var link = function (scope, element, attrs) {
            var parameters = {};

            scope.datepicker = {
                showDatepicker: false,
                showLoader: false,
                showMonthModal: false,
                showYearModal: false,
                showTodayButton: false,
                calendarMode: false,
                hideCancelButton: false,
                hideSetButton: false
            };

            parameters = drawDatepicker(scope);
            scope.datepicker.showDatepicker = parameters.showDatepicker || parameters.calendarMode;
            scope.datepicker.calendarMode = parameters.calendarMode;
            scope.datepicker.hideCancelButton = parameters.hideCancelButton;
            scope.datepicker.hideSetButton = parameters.hideSetButton;
            scope.datepicker.showTodayButton = onezoneDatepickerService.showTodayButton(parameters);

            /* VERIFY IF TWO DATES ARE EQUAL */
            scope.sameDate = function (date, compare) {
                return onezoneDatepickerService.sameDate(date, compare);
            };

            /* SELECT DATE METHOD */
            scope.selectDate = function (date, isDisabled) {
                if (!isDisabled) {
                    scope.selectedDate = date;
                    scope.month = scope.createDatepicker(scope.selectedDate);
                    scope.currentMonth = angular.copy(date);
                    scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());

                    if (parameters.calendarMode || parameters.hideSetButton) {
                        setDate(scope, parameters);
                    }
                }
            };

            /* SELECT TODAY METHOD */
            scope.selectToday = function () {
                var date = new Date();
                scope.selectedDate = date;
                scope.month = scope.createDatepicker(scope.selectedDate);
                scope.currentMonth = angular.copy(date);
                scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());

                if (parameters.calendarMode || parameters.hideSetButton) {
                    setDate(scope, parameters);
                }
            };

            /* SELECT MONTH METHOD */
            scope.selectMonth = function (monthIndex) {
                scope.currentMonth = new Date(scope.currentMonth.getFullYear(), monthIndex, 1);
                scope.month = scope.createDatepicker(scope.currentMonth);
                scope.closeModals();
            };

            /* SELECT YEAR METHOD */
            scope.selectYear = function (year) {
                scope.currentMonth = new Date(year, scope.currentMonth.getMonth(), 1);
                scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                scope.closeYearModal();
            };

            /* NEXT MONTH METHOD */
            scope.nextMonth = function () {
                scope.currentMonth = new Date(scope.currentMonth.getFullYear(), scope.currentMonth.getMonth() + 1, 1);
                scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                scope.month = scope.createDatepicker(scope.currentMonth);
            };

            /* PREVIOUS MOTH METHOD */
            scope.previousMonth = function () {
                scope.currentMonth = onezoneDatepickerService.getPreviousMonth(scope.currentMonth);
                scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                scope.month = scope.createDatepicker(scope.currentMonth);
            };

            scope.swipeLeft = function () {
                if (!parameters.disableSwipe) {
                    scope.nextMonth();
                }
            };

            scope.swipeRight = function () {
                if (!parameters.disableSwipe) {
                    scope.previousMonth();
                }
            };

            /* CLOSE MODAL */
            scope.closeModals = function () {
                scope.datepicker.showMonthModal = false;
                scope.datepicker.showYearModal = false;
            };

            /* OPEN SELECT MONTH MODAL */
            scope.openMonthModal = function () {
                scope.datepicker.showMonthModal = true;
            };

            /* OPEN SELECT YEAR MODAL */
            scope.openYearModal = function () {
                scope.datepicker.showMonthModal = false;
                scope.datepicker.showYearModal = true;
            };

            /* CLOSE SELECT MONTH MODAL */
            scope.closeYearModal = function () {
                scope.datepicker.showYearModal = false;
                scope.datepicker.showMonthModal = true;
            };

            scope.showDatepicker = function () {
                if (!scope.datepicker.showDatepicker) {
                    showHideDatepicker(scope, true);
                }
            };

            scope.hideDatepicker = function () {
                showHideDatepicker(scope, false);
            };

            scope.setDate = function () {
                setDate(scope, parameters);
            };

            scope.$watch('datepickerObject.date', function (date) {
                if (!onezoneDatepickerService.sameDate(date, scope.selectedDate)) {
                    scope.selectedDate = date;
                    scope.month = scope.createDatepicker(scope.selectedDate);
                    scope.currentMonth = angular.copy(date);
                    scope.selectedYearSlide = onezoneDatepickerService.getActiveYearSlide(scope.yearSlides, scope.currentMonth.getFullYear());
                }
            });

            scope.$watch('datepickerObject.showDatepicker', function (value) {
                scope.datepicker.showDatepicker = value;
            });

            element.on("click", function ($event) {
                var target = $event.target;
                if (angular.isDefined(target) && angular.element(target).hasClass("show-onezone-datepicker")) {
                    scope.$apply(function () {
                        drawDatepicker(scope);
                        scope.showDatepicker();
                    });
                }
            });
        };

        return {
            restrict: 'AE',
            replace: true,
            transclude: true,
            link: link,
            scope: {
                datepickerObject: '=datepickerObject'
            },
            //templateUrl: 'lib/onezone-datepicker/src/templates/onezone-datepicker.html'
            templateUrl: 'onezone-datepicker.html'
        };
    }]);